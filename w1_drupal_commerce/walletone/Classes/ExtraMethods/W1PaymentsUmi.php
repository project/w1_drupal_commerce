<?php
namespace WalletOne\Classes\ExtraMethods;

class W1PaymentsUmi extends \WalletOne\Classes\StandardsMethods\W1Payments  {
  public $orderid;
      
  function __construct($config, $params = array(), $nameCms = '') {
    parent:: __construct($config, $params, $nameCms);
    
    $this->logger = \Logger::getLogger(__CLASS__);
  }
  
  public function getListPayments($name, $umiObject, $filename){
    $list_payments = include($filename);
    $i = 0;
    $payment = [];
    foreach ($list_payments as $list) {
      if (!empty($list['data'])) {
        foreach ($list['data'] as $sublist) {
          if (!empty($sublist['data'])) {
            foreach ($sublist['data'] as $key => $subsublist) {
              if($name == 'PTENABLED'){
                $id = $subsublist['id'] . '_en';
                if ($umiObject->$id == 1) {
                  $payment[] = $subsublist['id'];
                }
              }
              else{
                $id = $subsublist['id'] . '_dis';
                if ($umiObject->$id == 1) {
                  $payment[] = $subsublist['id'];
                }
              }
            }
          }
          else {
            if($name == 'PTENABLED'){
              $id = $sublist['id'] . '_en';
              if ($umiObject->$id == 1) { 
                $payment[] = $sublist['id'];
              }
            }
            else{
              $id = $sublist['id'] . '_dis';
              if ($umiObject->$id == 1) {
                $payment[] = $sublist['id'];
              }
            }
          }
        }
      }
    }
    return $payment;
  }
  
  /**
   * Creating an array with the data for payment.
   * 
   * @param array $settings
   *  Settings from module.
   * @param array $invoce
   *  Order data.
   * @return array
   */
  public function createFormArray($settings, $invoce){
    $fields = array();
    $fields['CMS'] = $this->numCms;
    $fields['WMI_CURRENCY_ID'] = $invoce->currencyId;
    $fields['WMI_DESCRIPTION'] = "BASE64:" . base64_encode(sprintf(w1OrderDescr, $invoce->orderId, $this->siteName));
    $fields['WMI_EXPIRED_DATE'] = date('Y-m-d\TH:i:s', time() + 10000000);
    $fields['WMI_SUCCESS_URL'] = $this->successUrl;
    $fields['WMI_FAIL_URL'] = $this->failUrl;
    $fields['WMI_MERCHANT_ID'] = $settings->merchantId;
    $fields['WMI_PAYMENT_AMOUNT'] = number_format($invoce->summ, 2, '.', '');
    $orderId = $invoce->orderId . (preg_match('/.cms$/ui', $_SERVER['HTTP_HOST']) !== false 
          || preg_match('/.walletone.com$/ui', $_SERVER['HTTP_HOST']) !== false ? '_'.$_SERVER['HTTP_HOST'] : '');
    $fields['WMI_PAYMENT_NO'] = $orderId;
    $fields['WMI_CULTURE_ID'] = $settings->cultureId;
    
    if(!empty($invoce->firstNameBuyer)){
      $fields['WMI_CUSTOMER_FIRSTNAME'] = $invoce->firstNameBuyer;
    }
    if(!empty($invoce->lastNameBuyer)){
      $fields['WMI_CUSTOMER_LASTNAME'] = $invoce->lastNameBuyer;
    }
    if(!empty($invoce->emailBuyer)){
      $fields['WMI_CUSTOMER_EMAIL'] = $invoce->emailBuyer;
      $fields['WMI_RECIPIENT_LOGIN'] = $invoce->emailBuyer;
    }
    if(!empty($settings->paymentSystemEnabled)){
      $fields['WMI_PTENABLED'] = $settings->paymentSystemEnabled;
    }
    if(!empty($settings->paymentSystemDisabled)){
      $fields['WMI_PTDISABLED'] = $settings->paymentSystemDisabled;
    }
    $fields['orderid'] = $this->orderid;
    
    //The sorting fields
    foreach ($fields as $name => $val) {
      if (is_array($val)) {
        usort($val, "strcasecmp");
        $fields[$name] = $val;
      }
    }
    uksort($fields, "strcasecmp");
    
    $fields["WMI_SIGNATURE"] = $this->createSignature($fields, $settings->signature, $settings->signatureMethod);
    return $fields;
  }
}
